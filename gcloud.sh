#!/bin/bash -x

PROJECT="cloud-gpu-1"
ZONE="us-central1-a"
INSTANCE="gpu-01"
USER="vojta"


# Create VM instance
if [ "${1}" == "create" ]; then
    gcloud compute instances create "${INSTANCE}" \
        --project "${PROJECT}" \
        --machine-type n1-normal-4 \
        --zone "${ZONE}" \
        --boot-disk-size 40GB --boot-disk-type pd-ssd \
        --accelerator type=nvidia-tesla-k80,count=1 \
        --image-family ubuntu-1604-lts --image-project ubuntu-os-cloud \
        --maintenance-policy TERMINATE --restart-on-failure \
        --metadata-from-file ssh-keys="${HOME}/.ssh/id_rsa.pub" \
        --tags "jupyter-server" \
         --metadata-from-file startup-script="startup.sh"
        --preemptible
fi

# Create firewall rule for Jupyter
if [ "${1}" == "fire" ]; then
    gcloud compute firewall-rules create "default-allow-jupyter" \
        --network "default" --allow tcp:8888 \
        --direction "ingress" --priority 65534 \
        --source-ranges 0.0.0.0/0 \
        --target-tags "jupyter-server"
fi

# Start VM instance
if [ "${1}" == "start" ]; then
    gcloud compute instances start "${INSTANCE}" \
        --project "${PROJECT}"
fi

# SSH login
if [ "${1}" == "ssh" ]; then
    gcloud compute ssh "${USER}@${INSTANCE}" \
        --project "${PROJECT}" \
        --zone "${ZONE}"
fi

# SCP copy
if [ "${1}" == "scp" ]; then
    gcloud compute scp --recurse "${2}" "${USER}@${INSTANCE}:${3}" \
        --project "${PROJECT}" \
        --zone "${ZONE}"
fi

# Stop VM instance
if [ "${1}" == "stop" ]; then
    gcloud compute instances stop "${INSTANCE}" \
        --project "${PROJECT}"
fi
