#!/bin/bash -x

docker build \
   --tag deep-learning-with-python \
   --file dockerfile \
   .

docker run \
   --volume ~/dogs_cats:/notebooks/dogs_cats \
   --publish 8888:8888 \
   deep-learning-with-python &
