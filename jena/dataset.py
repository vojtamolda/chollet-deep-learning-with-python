import os
import numpy as np

# Download link:
#  https://s3.amazonaws.com/keras-datasets/jena_climate_2009_2016.csv.zip


def jena_dataset():
    with open('./jena/jena_climate_2009_2016.csv') as f:
        data = f.read()

    lines = data.split('\n')
    header = lines[0].split(',')
    lines = lines[1:]

    data = np.zeros((len(lines), len(header) - 1))
    for i, line in enumerate(lines):
        values = [float(x) for x in line.split(',')[1:]]
        data[i, :] = values
    return header, data
