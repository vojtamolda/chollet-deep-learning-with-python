#!/bin/bash -x

# nVidia CUDA
curl -s -L http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub \
    | sudo apt-key add -
curl -s -O https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
sudo dpkg --install cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
rm cuda-repo-ubuntu1604_9.1.85-1_amd64.deb

# Docker CE
curl -s -L https://download.docker.com/linux/ubuntu/gpg \
    | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"

# nVidia Docker
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey \
    | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/ubuntu16.04/nvidia-docker.list \
    | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt update
sudo apt -y upgrade
sudo apt -y --no-install-recommends install cuda-drivers
sudo apt -y --no-install-recommends install nvidia-docker2
sudo usermod -aG docker ${USER}
