import os
import numpy as np

# Download links:
#  http://ai.stanford.edu/~amaas/data/sentiment/
#  https://nlp.stanford.edu/projects/glove/


def imdb_dataset():
    imdb_dir = 'embeddings/train'
    labels = []
    texts = []

    for label_type in ['neg', 'pos']:
        dir_name = os.path.join(imdb_dir, label_type)
        for fname in os.listdir(dir_name):
            if fname[-4:] == '.txt':
                f = open(os.path.join(dir_name, fname))
                texts.append(f.read())
                f.close()
                if label_type == 'neg':
                    labels.append(0)
                else:
                    labels.append(1)
    return texts, labels


def glove_embedding():
    glove_dir = 'embeddings/glove'
    embeddings_index = {}
    with open(os.path.join(glove_dir, 'glove.6B.100d.txt')) as f:
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
    return embeddings_index
